package com.example.sungbo.databaseexample;

import android.content.Intent;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.sungbo.databaseexample.Model.TargetHistory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ThrowOnExtraProperties;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TargetActivity extends AppCompatActivity {

    private ProgressBar straight_progressbar;
    private ProgressBar jab_progressbar;
    private ProgressBar hook_progressbar;
    private ProgressBar uppercut_progressbar;

    private TextView straight_textview;
    private TextView jab_textview;
    private TextView hook_textview;
    private TextView uppercut_textview;

    private Button target_start_button;

    private int straight_count;
    private int jab_count;
    private int hook_count;
    private int uppercut_count;

    private int straight_thrown =0;
    private int jab_thrown =0;
    private int hook_thrown= 0;
    private int uppercut_thrown =0;

    private int time;


    private Chronometer chronometer;

    private DatabaseReference mDatabase;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mFirebaseAuth;
    private String uid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_target);

        straight_progressbar =findViewById(R.id.straight_progressbar);
        jab_progressbar =findViewById(R.id.jab_progressbar);
        hook_progressbar =findViewById(R.id.hook_progressbar);
        uppercut_progressbar =findViewById(R.id.uppercut_progressbar);

        straight_textview = findViewById(R.id.straight_progress);
        jab_textview = findViewById(R.id.jab_progress);
        hook_textview = findViewById(R.id.hook_progress);
        uppercut_textview = findViewById(R.id.uppercut_progress);

        target_start_button = findViewById(R.id.target_start);

        chronometer = findViewById(R.id.chronometer);

        Intent intent = getIntent();

        straight_count = intent.getIntExtra("straight", 0);
        jab_count = intent.getIntExtra("jab", 0);
        hook_count = intent.getIntExtra("hook", 0);
        uppercut_count = intent.getIntExtra("uppercut", 0);


        straight_textview.setText(String.valueOf(straight_count));
        jab_textview.setText(String.valueOf(jab_count));
        hook_textview.setText(String.valueOf(hook_count));
        uppercut_textview.setText(String.valueOf(uppercut_count));

        chronometer.setCountDown(true);

        time = intent.getIntExtra("duration", 0);

        chronometer.setBase(SystemClock.elapsedRealtime()  + ((time) * 1000 ));
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometerChanged) {
                chronometer = chronometerChanged;

                if(chronometer.getText().equals("00:00")){
                    chronometer.stop();
                }
            }


        });

        target_start_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(TargetActivity.this, PopActivity.class), 1);
            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1){
            if(resultCode==RESULT_OK){
                chronometer.setBase(SystemClock.elapsedRealtime()  + ((time) * 1000 ));
                chronometer.start();

                mFirebaseAuth = FirebaseAuth.getInstance();
                mFirebaseUser = mFirebaseAuth.getCurrentUser();
                uid = mFirebaseUser.getUid();

                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

                int punchesTargeted = straight_count + jab_count + hook_count + uppercut_count;
                int punchesThrown = straight_thrown + jab_thrown + hook_thrown + uppercut_thrown;
                int year = Calendar.getInstance().get(Calendar.YEAR);
                int month = Calendar.getInstance().get(Calendar.MONTH + 1);
                int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);

                TargetHistory tempHistory = new TargetHistory(punchesTargeted, punchesThrown, year, month, day, time);

                if( punchesTargeted == 0 || punchesThrown == 0){
                    tempHistory.setCompletion(0);
                }
                else if(punchesThrown == 0){
                    tempHistory.setCompletion( punchesThrown / punchesThrown * 100);
                }

                FirebaseDatabase.getInstance().getReference().child("users")
                        .child(uid).child("targetHistory")
                        .child(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)))
                        .child(String.valueOf(Calendar.getInstance().get(Calendar.MONTH) + 1))
                        .child(sdf.format(cal.getTime())).setValue(tempHistory);

            }
        }
    }

    public void setProgressBar(){
        if(straight_thrown != 0 && straight_count != 0)
            straight_progressbar.setProgress( straight_thrown / straight_count * 100, true);
        if(jab_thrown != 0 && jab_count != 0)
            jab_progressbar.setProgress( jab_thrown / jab_count * 100, true);
        if(hook_thrown != 0 && hook_count != 0)
            hook_progressbar.setProgress( hook_thrown / hook_count * 100, true);
        if(uppercut_thrown != 0 && uppercut_count != 0)
            uppercut_progressbar.setProgress( uppercut_thrown / uppercut_count * 100,true);
    }
}
